package md5dfdb3197611a246749a6cb2aad1c8ca7;


public class MyChallenges
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("TaskyAndroid.Screens.MyChallenges, TaskyAndroid, Version=1.0.5781.30757, Culture=neutral, PublicKeyToken=null", MyChallenges.class, __md_methods);
	}


	public MyChallenges () throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyChallenges.class)
			mono.android.TypeManager.Activate ("TaskyAndroid.Screens.MyChallenges, TaskyAndroid, Version=1.0.5781.30757, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
